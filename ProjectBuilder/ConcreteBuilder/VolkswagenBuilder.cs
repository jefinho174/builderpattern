﻿using System;
using ProjectBuilder.Builders;
using System.Collections.Generic;
using System.Text;

namespace ProjectBuilder.ConcreteBuilder
{
    public class VolkswagenBuilder : CarroBuilder
    {
        public override void buildAnoFabricacao()
        {
            this.carro.anoFabricacao = 2017;
        }

        public override void buildDscMotor()
        {
            this.carro.dscMotor = "Gol Flex 1.0";
        }

        public override void buildModelo()
        {
            this.carro.modelo = "Gol 16v";
        }

        public override void buildMontatora()
        {
            this.carro.montatora = "Volkswagen";
        }

        public override void buildPreco()
        {
            this.carro.preco = 11000.0;
        }
    }
}
