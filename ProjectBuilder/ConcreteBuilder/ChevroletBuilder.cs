﻿using System;
using ProjectBuilder.Builders;
using System.Collections.Generic;
using System.Text;

namespace ProjectBuilder.ConcreteBuilder
{
    public class ChevroletBuilder : CarroBuilder
    {
        public override void buildAnoFabricacao()
        {
            this.carro.anoFabricacao = 1988;
        }

        public override void buildDscMotor()
        {
            this.carro.dscMotor = "Chevette CL 1.6";
        }

        public override void buildModelo()
        {
            this.carro.modelo = "Chevette CL";
        }

        public override void buildMontatora()
        {
            this.carro.montatora = "Chevrolet";
        }

        public override void buildPreco()
        {
            this.carro.preco = 4000.0;
        }
    }
}
