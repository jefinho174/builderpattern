﻿using ProjectBuilder.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectBuilder.Builders
{
    public abstract class CarroBuilder
    {
        protected Carro carro;
        public CarroBuilder()
        {
            carro = new Carro();
        }

        public abstract void buildPreco();
        public abstract void buildDscMotor();
        public abstract void buildAnoFabricacao();
        public abstract void buildModelo();
        public abstract void buildMontatora();

        public Carro GetCarro()
        {
            return carro;
        }

    }
}
