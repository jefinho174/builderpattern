﻿using System;
using ProjectBuilder.ConcreteBuilder;
using ProjectBuilder.Directors;

namespace ProjectBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            ConcessionariaDirector c1 = new ConcessionariaDirector(new VolkswagenBuilder());
            c1.construirCarro();
            c1.Show();

            ConcessionariaDirector c2 = new ConcessionariaDirector(new ChevroletBuilder());
            c2.construirCarro();
            c2.Show();
            Console.ReadKey();
        }
    }
}
