﻿using ProjectBuilder.Builders;
using ProjectBuilder.Products;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectBuilder.Directors
{
    class ConcessionariaDirector
    {
        protected CarroBuilder montadora;

        public ConcessionariaDirector(CarroBuilder carroBuilder)
        {
            this.montadora = carroBuilder;
        }

        public void construirCarro()
        {
            montadora.buildPreco();
            montadora.buildModelo();
            montadora.buildAnoFabricacao();
            montadora.buildDscMotor();
            montadora.buildMontatora();
        }

        public void Show()
        {
            Carro vCarro = this.montadora.GetCarro();

            Console.WriteLine("\n---------------------------");
            Console.WriteLine(" Modelo : {0}", vCarro.modelo);
            Console.WriteLine(" Ano de Fabricacao : {0}", vCarro.anoFabricacao);
            Console.WriteLine(" Descricao do Motos: {0}", vCarro.dscMotor);
            Console.WriteLine(" Montadora : {0}", vCarro.montatora);
            Console.WriteLine(" Preco : {0}", vCarro.preco);
        }

    }
}
